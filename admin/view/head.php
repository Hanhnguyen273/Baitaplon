<!-- Tham khảo giao diện trang course.uet.vnu.edu.vn -->
<style type="text/css">
    .modal-header, h4, .close {
        background: #222;
        color: #ffffff;
        font-size: 18px;
    }
    .modal-footer {
      background-color: #f9f9f9;
  }
  .form{
    border: none;
    border-radius: 0px;
    margin-bottom: 0px;
    color: #222;
    background-color: white;
    font-size: 13px;
    margin: 0;
    outline: none;
    padding: 4px 2px 4px 2px;
    resize: none;
    -webkit-box-sizing: border-box;
    width: 100%;
    height: 50px;
    border-bottom: 1px solid #ccc;"

}
</style>
<?php
include('../../system/config/connect.php');
include('../../site/model/user.php');
if(isset($_POST['ok'])){
    $mTo = $_POST['mTo'];
    $nTo = substr($mTo,0,7);
    $title = $_POST['title'];
    $content = $_POST['content'];
    $chk = sendmail($title,$content,$nTo,$mTo);
    if($chk == 1){
        echo "<script>alert('Gửi mail thành công')</script>";
    }else{
        echo "<script>alert('Lỗi!')</script>";
    }
}
?>
<header >   
    <div class="container" style ="background: #88b77b; width: 100%;height: 120px;">
        <div class="row">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img src="../../public/images/uet_logo.png" style="ma"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse" style="margin-top:27px;">
                        <ul class="nav navbar-nav">
                            <li ><a href="http://localhost/Baitaplon/admin/view/me.php" style="font-size: 25px; color: #ffffff; font-weight: bold;text-shadow: 2px 2px 2px #cc0000;">ThisisMgr</a></li>
                            <li><i class="fa fa-graduation-cap" style="font-size:40px;color: #cc0000; margin-top:10px;"></i></li>
                        </ul>
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown" id="menu_set">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 18px; color: #ffffff;
                                background: none; text-decoration: none;"><?php echo $_SESSION['hoten'];?><b class="caret" style="font-size: 25px;"></b></a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuDivider">
                                    <li><a href="http://localhost/Baitaplon/admin/view/me.php"><i class="fa fa-home"></i> ThesisMgr</a></li>
                                    <li  role="separator" class="divider"></li>
                                    <li><a href="../controller/profile.php"><i class="fa fa-user"></i> Profile</a></li>
                                    <li><a href="#" id="myBtn"><i class="fa fa-commenting-o"></i>Messages</a></li>
                                    <li><a href="" id="ngu" "><i class="fa fa-cog fa-spin" style="font-size:16px"></i> Preferences</a></li>

                                    <li  role="separator" class="divider"></li>
                                    <li><a href="../controller/logout.php"><i class="fa fa-unlock-alt"></i> Log out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </nav>
        </div>
    </div>

    <div class="content">

        <div class="row" style="margin:25px; margin-bottom: 0;">
            <a href="../../admin/view/me.php" style="font-size: 18px;
            color: #828282; float: left;
            display: inline-block;"><i class="fa fa-caret-right" style="color: #ff6c00;"></i>Home</a>
            <div class="text-right" id="diachi">
                <span style="font-size: 18px;
                color: #828282;
                display: inline-block;"><i class="fa fa-phone" style="color: #ff6c00;"></i>Call  US: (09) 8554 1627</span>
                <span style="font-size: 18px;
                color: #828282;
                display: inline-block;"><i class="fa fa-envelope-o" style="color: #ff6c00;"></i>Email: ngannt1710@gmail.com</span>
            </div>              
        </div>
    </div>
    
</header>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style=" width: 510px">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-lable="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><span class="glyphicon glyphicon-envelope"></span>  Thư mới</h4>
      </div>
      <div class="modal-body" style="padding: 0px;">
          <form role="form" method="post">
            <div class="form-group">
              <!-- <input type="text" class="form" id="mTo" name="mTo" placeholder="Người nhận"> -->
              <input type="email"  id="email" class="form" name="mTo" tabindex="2" placeholder="Người nhận" required>
              <input type="text" class="form" id="title" name="title" placeholder="Chủ đề">

              <textarea name ="content"  id ="editer" style="width: 100%; min-height: 280px;"></textarea>
          </div>

          
      </div>
      <div class="modal-footer">
          <p type="submit" class="pull-left" data-dismiss="modal"><span class=" fa fa-bitbucket" style="font-size: 24px;"></span></p>
          <p><button type="submit" name = "ok" style="background: #222; color: #ffffff; width: 50px;">Gửi</button></p>
      </div>
  </form>
</div>

</div>
</div> 
<script>
    $(document).ready(function(){
        $("#myBtn").click(function(){
            $("#myModal").modal();
        });
    });
</script>

