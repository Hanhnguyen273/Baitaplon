drop database web;
create database web DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
use web;
create table khoa(
	idkhoa varchar(100),
	vpk varchar(100) NOT NULL,
	tenkhoa varchar(100) NOT NULL,
	PRIMARY KEY (idkhoa)
)ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table linhvuc(
	idlinhvuc varchar(100) NOT NULL,
	tenlinhvuc varchar(100) NOT NULL,
	idkhoa varchar(100) NOT NULL,
	PRIMARY KEY(idlinhvuc),
	CONSTRAINT fk_linhvuc_khoa FOREIGN KEY (idkhoa) REFERENCES khoa(idkhoa)  ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table giaovien(
	idgiaovien int(11),
	idkhoa varchar(100) NOT NULL,
	hoten varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	pass varchar(100) NOT NULL,
	PRIMARY KEY(idgiaovien),
	CONSTRAINT fk_giaovien_khoa FOREIGN KEY (idkhoa) REFERENCES khoa(idkhoa) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table bomon(
	idgiaovien int(11),
	idbomon int(11) AUTO_INCREMENT,
	idlinhvuc varchar(100) NOT NULL,
	tenbomon varchar(100) NOT NULL,
	PRIMARY KEY(idbomon),
	CONSTRAINT fk_bomon_giaovien FOREIGN KEY (idgiaovien) REFERENCES giaovien(idgiaovien) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT fk_bomon_linhvuc FOREIGN KEY (idlinhvuc) REFERENCES linhvuc(idlinhvuc) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table sinhvien(
	idsv int(11) PRIMARY KEY,
	idkhoa varchar(100) NOT NULL,
	hoten varchar(100) NOT NULL,
	chksvbaove boolean NOT NULL,
	khoahoc varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	pass varchar(100) NOT NULL,
	CONSTRAINT fk_sinhvien_khoa FOREIGN KEY (idkhoa) REFERENCES khoa(idkhoa) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table detai(
	iddetai int(11) AUTO_INCREMENT,
	idgiaovien int(11),
	idsv int(11),
	tendetai varchar(100) NOT NULL,
	chitiet varchar(200) NOT NULL,
	idlinhvuc varchar(100) NOT NULL,
	PRIMARY KEY (iddetai,idsv),
	chk boolean NOT NULL,
	CONSTRAINT fk_detai_giaovien FOREIGN KEY (idgiaovien) REFERENCES giaovien(idgiaovien) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT fk_detai_linhvuc FOREIGN KEY (idlinhvuc) REFERENCES linhvuc(idlinhvuc) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table admin(
	hoten varchar(100) NOT NULL,
	user varchar(100) PRIMARY KEY,
	pass varchar(100) NOT NULL
)ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;