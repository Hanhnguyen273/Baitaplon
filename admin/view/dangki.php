<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Đăng kí đề tài</title>
	<link rel="stylesheet" type="text/css" href="../../public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/mystyle.css">
	<script type="text/javascript" src="../../public/js/jquery-2.2.4.js"></script>
	<script type="text/javascript" src="../../public/js/bootstrap.min.js"></script>
	<!-- bs3-cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>
	<?php
	include('head.php');
	?>
	<section style="width: 100%;" >
		<div class="container" style="width: 100%;">
			<div class="row" style="width: 100%;">
				<div class="col-md-2"></div>
				<div class="col-md-8"  id ="linhvuc_set" style="border: 1px solid #88b77b; margin-top: 50px;">
					<div class="tieude">
						<h2 style="font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;font-size: 28px; color: #f60;" class="text-center">Đăng ký đề tài</h2>	
					</div>
					<div class="content_one">
						<form action="" method="post" enctype="multipart/form-data">
							<table class="table bang">
								<tbody>
									<tr>
										<td style="font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;font-size: 18px; color: #f60;">Chi tiết: </td>
										<td>
											<textarea name ="chitiet" rows="10" cols="100" id ="editer" placeholder="Bạn muốn làm gì với đề tài này......"></textarea>
											<script type="text/javascript">CKEDITOR.replace( 'editer');</script>
										</td>
									</tr>
									<tr>
										<td></td>
										<td><input type="submit" name="ok" value="Đăng ký" style="color: #ffffff; background: #88b77b">
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
				<div class="col-md-2">

				</div>
			</div>
		</div>
	</section>
	<?php
	include('footer.php');
	?>
</body>
</html>