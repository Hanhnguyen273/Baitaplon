<!DOCTYPE html>
<html lang="en">
<head>
  <title>Export</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../../public/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../../public/css/mystyle.css">
  <script type="text/javascript" src="../../public/js/jquery-2.2.4.js"></script>
  <script type="text/javascript" src="../../public/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script language="javascript" src="../../system/library/ckeditor/ckeditor.js" type="text/javascript"></script>
</head>
<body>
  <form name="export_form" action="" method="post">
  <a href="../view/admin.php"><button type="button" class= "btn btn-block" style="background: #5f5f5f; text-align: left; color: #ffffff;"><i class="fa fa-long-arrow-left" style="color: #ffffff"></i>Go back to Admin Area</button></a>
   <div class="dropdown"  style="text-align:right;margin-top: 15px;margin-right: 15px;">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="background: #5f5f5f; color: #ffffff;">Export as to
      <span class="caret"></span></button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li > <input class="btn btn-default" type="submit" name="submit_docs" value="Export as MS Word" class="input-button" class="text-right" /></li>
        <li class="divider"></li>
        <li><input class="btn btn-default" type="submit" name="ExportType" value="Export as MS Excel"></li>
      </ul>
    </div>

    <br>
    <textarea name ="content"   id ="editer" placeholder="Nhập nội dung........"></textarea>
    <script type="text/javascript">CKEDITOR.replace( 'editer'); </script>

    <table class="table table-striped" id="student">
      <tr>
       <th>STT</th><th>MSSV</th><th>Họ tên</th><th>Khóa học</th><th>Đề tài</th><th>Giáo viên hướng dẫn</th>
     </tr>
     <?php databaseOutput(); 

     ?>

   </table>
 </form>
</body>
</html>