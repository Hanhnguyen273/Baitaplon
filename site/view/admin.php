<?php session_start(); 
//Kiểm tra nếu chưa dăng nhập thì đăng nhập
if($_SESSION['loged'] == 0){
	header('location:../../admin/controller/login.php');
}else {
	//kiểm tra nếu không phải là admin thì k đc vào trang này
	if($_SESSION['user'] != "admin"){
		echo("Trang này không tồn tại");
	}else{

		?>
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>ThesisMgr</title>
			<link rel="stylesheet" type="text/css" href="../../public/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="../../public/css/mystyle.css">
			<script type="text/javascript" src="../../public/js/jquery-2.2.4.js"></script>
			<script type="text/javascript" src="../../public/js/bootstrap.min.js"></script>
			<!-- bs3-cdn -->
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
			<style type="text/css">
				.modal-header, h4, .close {
					background: #222;
					color: #ffffff;
					font-size: 18px;
				}
				.modal-footer {
					background-color: #f9f9f9;
				}
				.form{
					border: none;
					border-radius: 0px;
					margin-bottom: 0px;
					color: #222;
					background-color: white;
					font-size: 13px;
					margin: 0;
					outline: none;
					padding: 4px 2px 4px 2px;
					resize: none;
					-webkit-box-sizing: border-box;
					width: 100%;
					height: 50px;
					border-bottom: 1px solid #ccc;"

				}
			</style>
		</head>
		<body>


			<?php
//Xử lý gửi mail người dùng
			include('../../system/config/connect.php');
			include('../../site/model/user.php');
			if(isset($_POST['ok'])){
				$mTo = $_POST['mTo'];
				$nTo = substr($mTo,0,7);
				$title = $_POST['title'];
				$content = $_POST['content'];
				$chk = sendmail($title,$content,$nTo,$mTo);
				if($chk == 1){
					echo "<script>alert('Gửi mail thành công')</script>";
				}else{
					echo "<script>alert('Lỗi!')</script>";
				}
			}
			?>
			<!-- Modal gửi email -->
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content" style=" width: 510px">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-lable="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title"><span class="glyphicon glyphicon-envelope"></span>  Thư mới</h4>
						</div>
						<div class="modal-body" style="padding: 0px;">
							<form role="form" method="post">
								<div class="form-group">
									<!-- <input type="text" class="form" id="mTo" name="mTo" placeholder="Người nhận"> -->
									<input type="email"  id="email" class="form" name="mTo" tabindex="2" placeholder="Người nhận" required>
									<input type="text" class="form" id="title" name="title" placeholder="Chủ đề">

									<textarea name ="content"  id ="editer" style="width: 100%; min-height: 280px;"></textarea>
								</div>


							</div>
							<div class="modal-footer">
								<p type="submit" class="pull-left" data-dismiss="modal"><span class="fa fa-bitbucket" style="font-size: 24px;"></span></p>
								<p><button type="submit" name = "ok" id="btn" style="background: #222; color: #ffffff">Gửi</button></p>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>


		<script>
			$(document).ready(function(){
				$("#myBtn").click(function(){
					$("#myModal").modal();
				});
			});
		</script>
		<header >	
			<div class="container" style ="background: #88b77b; width: 100%;height: 120px;">
				<div class="row">
					<nav class="navbar navbar-default" role="navigation">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<a class="navbar-brand" href="#"><img src="../../public/images/uet_logo.png" style="ma"></a>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse navbar-ex1-collapse" style="margin-top:27px;">
								<ul class="nav navbar-nav">
									<li ><a href="http://localhost/Baitaplon/admin/view/me.php" style="font-size: 25px; color: #ffffff; font-weight: bold;
										text-shadow: 2px 2px 2px #cc0000;">ThisisMgr</a></li>
										<li><i class="fa fa-graduation-cap" style="font-size:40px;color: #cc0000; margin-top:10px;"></i></li>
									</ul>
									<form class="navbar-form navbar-left" role="search">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Search">
										</div>
										<button type="submit" class="btn btn-default">Submit</button>
									</form>
								<ul class="nav navbar-nav navbar-right">							<!-- 
									<li><button type="button" onclick="loadDoc()" id="login_set">Change Content</button></li> -->
									<li class="dropdown" id="menu_set">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 18px; color: #ffffff;background: none;"><?php echo $_SESSION['hoten'];?><b class="caret"></b></a>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuDivider">
											<li><a href="../../admin/view/me.php"><i class="fa fa-home"></i> ThesisMgr</a></li>
											<li  role="separator" class="divider"></li>
											<li><a href="../../admin/controller/profile.php"><i class="fa fa-user"></i> Profile</a></li>
											<li><a href="#" id="myBtn"><i class="fa fa-commenting-o"></i>Messages</a></li>
											<li><a href="" id="ngu" "><i class="fa fa-cog fa-spin" style="font-size:16px"></i> Preferences</a></li>

											<li  role="separator" class="divider"></li>
											<li><a href="../../admin/controller/logout.php"><i class="fa fa-unlock-alt"></i> Log out</a></li>
										</ul>
									</li>
								</ul>
							</div><!-- /.navbar-collapse -->
						</div>
					</nav>
				</div>
			</div>
			<div class="content">
				<div class="row" style="margin-top:25px;">
					<div >
						<a href="../../admin/view/me.php" style="font-size: 18px;
						color: #828282;
						display: inline-block;"><i class="fa fa-caret-right" style="color: #ff6c00;"></i>Home</a>
						<a href="" style="font-size: 18px;
						color: #828282;
						display: inline-block;"><i class="fa fa-caret-right" style="color: #ff6c00;"></i>Dashboard</a>
					</div>              
				</div>
			</div>
		</header>

		<div class="container" style="width: 100%">
			<div class="row">
				<div class="col-md-8" style="border: 1px solid #88b77b; margin: 50px;">
					<div>
						<h3 style="color: #f60; padding: 20px; font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Quản lý sinh viên</h3>
					</div>
					<form enctype="multipart/form-data" action="../controller/upload_sinhvien.php" method="post"> 
						<input type="hidden" name="MAX_FILE_SIZE" value="2000000"/> 
						<table class="table table-bordered">
							<tbody>
								<tr>
									<td style="width: 30%"><strong>Danh sách sinh viên</strong>:</td> 
									<td style="width: 60%"><input type="file" name="file1" /></td> 
									<td><input type="submit" value="Upload" /></td> 
								</tr>
							</tbody>
						</table>
					</form> 
					<form enctype="multipart/form-data" action="../controller/upload_giaovien.php" method="post"> 
						<input type="hidden" name="MAX_FILE_SIZE" value="2000000"/> 
						<table class="table table-bordered">
							<tbody>
								<tr> 
									<td style="width: 30%"><strong>Danh sách giáo viên</strong>:</td> 
									<td style="width: 60%"><input type="file" name="file2" /></td> 
									<td><input type="submit" value="Upload" /></td> 
								</tr>
							</tbody>
						</table>
					</form> 	
					<form enctype="multipart/form-data" action="../controller/upload_svbaove.php" method="post"> 
						<input type="hidden" name="MAX_FILE_SIZE" value="2000000"/> 
						<table class="table table-bordered">
							<tbody>
								<tr> 
									<td style="width: 30%"><strong>Danh sách sv đăng kí</strong>:</td> 
									<td style="width: 60%"><input type="file" name="file3" /></td> 
									<td><input type="submit" value="Upload" /></td> 
								</tr> 
							</tbody>
						</table>
					</form>
				</div>
				<div class="col-md-3">
					<script>

						function hidework(){
							document.getElementById("chanvai").style.display = "none";
						}
						function showwork(){
							document.getElementById("chanvai").style.display = "";
						}
						function hidecontent(){
							document.getElementById("linhvuc_set").style.display = "none";
							document.getElementById("giaovien_set").style.display = "none";
							document.getElementById("donvi_set").style.display = "none";
							document.getElementById("linhvuc").className = "noselect";
							document.getElementById("giaovien").className = "noselect";
							document.getElementById("donvi").className = "noselect";
						}
						function showcontent(p){
							hidecontent();
							document.getElementById(p+"_set").style.display = "";
							document.getElementById(p).className = "qlyclick";
						}
						showcontent('linhvuc');

					</script>
					<table class="table table-bordered" style="margin-top: 50px;" id="an">
						<thead>
							<tr>
								<th style="font-size: 18px; background: #88b77b; color: #ffffff;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Danh sách công việc: </th>
							</tr>
						</thead>
						<tbody>
							<tr id="export">
								<td> <div  id ="export"> <a href="../controller/export.php" id="qly" ><i class="fa fa-arrow-circle-o-right" style="font-size:24px;color:#c0392b;"></i>Xuất công văn</a></div></td>
							</tr>

						</tbody>
					</table>

				</div>
			</div>
			<script type="text/javascript">
				function getConfirm(){
					var retVal = confirm("Do you want to continue ?");
					if( retVal == true ){
						document.write ("User wants to continue!");
						return true;
					}
					else{
						document.write ("User does not want to continue!");
						return false;
					}
				}
         //-->
     </script>
     <div class="row">
     	<div class="col-md-8" style="border: 1px solid #88b77b; margin: 50px;">
     		<div>
     			<h3 style="color: #f60; padding: 20px; font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Gửi email thông báo
     			</h3>
     		</div>
     		<form action="../controller/sendmail.php" method="post">
     			<table class="table table-bordered">
     				<tbody>
     					<tr>
     						<td style="width: 70% style="width: 30%"">Gửi mail tới giáo viên: </td>
     						<td><input type="submit" name="submit1" value="Send" onclick="javascript:getConfirm();"></td>
     					</tr>
     				</tbody>
     			</table>						
     		</form>
     		<form action="../controller/sendmail.php" method="post">
     			<table class="table table-bordered">
     				<tbody>
     					<tr>
     						<td style="width: 70%">Gửi mail tới sinh viên: </td>
     						<td><input type="submit" name="submit2" value="Send" onclick="javascript:getConfirm();"></td>
     					</tr>
     				</tbody>
     			</table>						
     		</form>
     		<form action="../controller/sendmail.php" method="post">
     			<table class="table table-bordered">
     				<tbody>
     					<tr>
     						<td style="width: 70%">Gửi mail tới sinh viên bảo vệ: </td>
     						<td><input type="submit" name="submit3" value="Send" onclick="javascript:getConfirm();"></td>
     					</tr>
     				</tbody>
     			</table>						
     		</form>
     		<form action="../controller/sendmail.php" method="post">
     			<table class="table bang">
     				<thead>
     					<tr>
     						<th class = 'qlyclick' style="border: none;" colspan="2">Thông báo</th>
     					</tr>
     				</thead>
     				<tbody>
     					<tr>
     						<td style="color: #88b77b; font-size: 18px;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif; ">Gửi tới: </td>
     						<td>
     							<select name="guitoi" style="height: 40px;">
     								<option value="sinhvienbaove">Sinh viên bảo vệ</option>
     								<option value="sinhvien">Sinh viên</option>
     								<option value="giaovien">Giáo viên</option>
     							</select>
     						</td>
     					</tr>
     					<tr>
     						<tr>
     							<td style="color: #88b77b; font-size: 18px;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif; ">Nội dung: </td>
     							<td>
     								<div><input type="text" name="title" placeholder="Tiêu đề" style="width: 605px; height: 50px"></div>
     								<textarea name ="content" rows="8" cols="83" id ="editer" placeholder="Nhập nội dung........"></textarea>
     								<script type="text/javascript">CKEDITOR.replace( 'editer');</script>
     							</td>
     						</tr>

     						<td></td>
     						<td><button type="submit" name = "submit4">Gửi</button></td>
     					</tr>
     				</tbody>
     			</table>
     		</form>

     	</div>	

     </div>
 </div>
 <?php
 if($_SESSION['chkmail'] == 1){
 	echo "<script>alert('Gửi mail thành công');</script>";
 	$_SESSION['chkmail'] =0;
 }
 ?>

 <?php
 include('../../admin/view/footer.php');
 ?>
</body>
</html>
<?php
}
}
?>